
/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-cam-post-image-photo-server/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
*/

#include <Arduino.h>
#include <WiFi.h>

#include <WiFiClient.h>

#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "esp_camera.h"

const char* ssid = "A1212";
const char* password = "123456789";

//String serverName = "18.216.25.161";   // Faceometric
//String serverName = "3.14.34.174";   // Parth Server

//String serverName = "18.117.236.142";   // Jay
String serverName = "18.221.122.94";   // Sir

String serverPath = "/verify";     // /verify The default serverPath should be upload.php
//String serverPath = "/postTest/test_fullraw.php";

const int serverPort = 5000;
//const int serverPort = 80;

WiFiClient client;

// CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

const int timerInterval = 30000;    // time between each HTTP POST image
unsigned long previousMillis = 0;   // last time image was sent

void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); 
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("ESP32-CAM IP Address: ");
  Serial.println(WiFi.localIP());

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;

  // init with high specs to pre-allocate larger buffers
  if(psramFound()){
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 10;  //0-63 lower number means higher quality
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_CIF;
    config.jpeg_quality = 12;  //0-63 lower number means higher quality
    config.fb_count = 1;
  }
  
  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    delay(1000);
    ESP.restart();
  }

  sendPhoto(); 
}

void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= timerInterval) {
    sendPhoto();
    previousMillis = currentMillis;
  }
}

String sendPhoto() {
  String getAll;
  String getBody;

  camera_fb_t * fb = NULL;
  fb = esp_camera_fb_get();
  if(!fb) {
    Serial.println("Camera capture failed");
    delay(1000);
    ESP.restart();
  }
///////////////////////////////////////////
/*   
  char ch;
  char *filename = "/sdcard/1.jpg";
  
  Serial.println(filename);
  
  FILE *file = fopen(filename, "r");
  if (file != NULL)  {
    while((ch = fgetc(file)) != EOF)
      printf("%c", ch);

  }  else  {
    Serial.println("Could not open file");
  }
 // fclose(file);*/
  /////////////////////////////////////
  
  Serial.println("Connecting to server: " + serverName);
  
  //Serial.println(c_str());
  
  if (client.connect(serverName.c_str(), serverPort)) {
    Serial.println("Connection successful!");  
      
    String head = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"imageFile\"; filename=\"esp32-cam.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n";
 
  
  //String head = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"image\"; filename=\"esp32-cam.jpg\"\r\n";
    
   // String head = "--RandomNerdTutorials\r\nContent-Disposition: form-data; subject_id=\"568\"; gallery_id=\"AWALPLASTICS\";req_time=\"1111\"; name=\"imageFile\"; filename=\"esp32-cam.jpg\"\r\nContent-Type: image/jpeg\"\r\n\r\n";
 
  //subject_id=568&gallery_id=AWALPLASTICS&req_time=1111&image=

  /*
    String head1 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"subject_id\"; \r\n\r\n";

    String head2 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"gallery_id\"; \r\n\r\n";

    String head3 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"req_time\"; \r\n\r\n";

    */
    String head1 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"subject_id\"\r\n\r\n1785\r\n";

    String head2 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"gallery_id\"\r\n\r\nAWALPLASTICS\r\n";

    String head3 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"req_time\"\r\n\r\n1111\r\n";

    
   /* 
    String head1 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"subject_id\"\r\n1785";

    String head2 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"gallery_id\"\r\nAWALPLASTICS";

    String head3 = "--RandomNerdTutorials\r\nContent-Disposition: form-data; name=\"req_time\"\r\n1111";
   
    */
    
    String tail = "--RandomNerdTutorials--\r\n";

    uint32_t imageLen = fb->len;
    
    uint32_t extraLen = head1.length() + head2.length() + head3.length() + head.length() + tail.length();

    //uint32_t extraLen = /*head.length() + head1.length() + head3.length()*/ + head2.length() + tail.length();
    
    uint32_t totalLen = imageLen + extraLen;

  
    //uint32_t totalLen = /*imageLen +*/ extraLen;

    
    Serial.print("\r\nPOST " + serverPath + " HTTP/1.1");
  
    client.print("\r\nPOST " + serverPath + " HTTP/1.1");
    
    //Serial.print("\r\nUser-Agent: PostmanRuntime/7.28.0");
    //client.print("\r\nUser-Agent: PostmanRuntime/7.28.0");
    
    Serial.print("\r\nAccept: */*");
    client.print("\r\nAccept: */*");
    

    //Serial.print("\r\nPostman-Token: 9a86e4b1-393d-4efd-a81c-f0c79c51e7ab");
   // client.print("\r\nPostman-Token: 9a86e4b1-393d-4efd-a81c-f0c79c51e7ab");
   /*
    Serial.print("\r\nHost: " + serverName);
    client.print("\r\nHost: " + serverName);

    */   
    Serial.print("\r\nHost: " + serverName + ":" + serverPort);
    client.print("\r\nHost: " + serverName + ":" + serverPort);
    
    
    //Serial.print("\r\nAccept-Encoding: gzip, deflate, br");   
    //client.print("\r\nAccept-Encoding: gzip, deflate, br");
    
    Serial.print("\r\nConnection: keep-alive");
    client.print("\r\nConnection: keep-alive");
  
   //Serial.println("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
   //client.println("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");

    Serial.print("\r\nContent-Type: multipart/form-data; boundary=RandomNerdTutorials");
    client.print("\r\nContent-Type: multipart/form-data; boundary=RandomNerdTutorials");
    
    
    Serial.print("\r\nContent-Length: " + String(totalLen) + "\r\n\r\n");
    client.print("\r\nContent-Length: " + String(totalLen) + "\r\n\r\n");
    
  
    Serial.print(head1);
    client.print(head1);
    
  

    Serial.print(head);
    client.print(head);
   
    uint8_t *fbBuf = fb->buf;
    size_t fbLen = fb->len;
    for (size_t n=0; n<fbLen; n=n+1024) {
      
      if (n+1024 < fbLen) {
        client.write(fbBuf, 1024);
        fbBuf += 1024;
      }
      else if (fbLen%1024>0) {
        size_t remainder = fbLen%1024;
        client.write(fbBuf, remainder);
      }
      
    }   
    



    Serial.print(head2);
    client.print(head2);
    


    Serial.print(head3);
    client.print(head3);

     
    Serial.print(tail);
    client.print(tail);

    
  //  fclose(file);
  
    esp_camera_fb_return(fb);
    
    int timoutTimer = 10000;
    long startTimer = millis();
    boolean state = false;
    
    while ((startTimer + timoutTimer) > millis()) {
      Serial.print(".");
      delay(100);      
      while (client.available()) {
        char c = client.read();
        if (c == '\n') {
          if (getAll.length()==0) { state=true; }
          getAll = "";
        }
        else if (c != '\r') { getAll += String(c); }
        if (state==true) { getBody += String(c); }
        startTimer = millis();
      }
      if (getBody.length()>0) { break; }
    }
    Serial.println();
    client.stop();
    Serial.println(getBody);
  }
  else {
    Serial.println("Connection not successful!"); 
    getBody = "Connection to " + serverName +  " failed.";
    Serial.println(getBody);
  }
  return getBody;
}
